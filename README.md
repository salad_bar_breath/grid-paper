# Grid Paper

## About These Files

"Graph\_Paper\_-\_Wide\_Grid.svg" and "Graph\_Paper\_-\_Narrow\_Grid.svg" are files created in Inkscape that are gridded drawings with 2D-Cartesian axes with tick marks. 
The tick marks are grouped together by even and odd groups so that one can easily thin them according to need. 
The page is set up in the first quadrant of the grid so vectors and objects can be created relative to the Cartesian plane. 
It took me a little while to put these together, so I thought it would be helpful to anyone that would need it.

This project is licensed with the Unlicense (see unlicense.org). This project is in the public domain, attribution appreciated but not required.

 - Salad Breath